baseurl = https://radiance.thinkmoult.com/

.PHONY : dist
dist :
	find ./www/lib/ -maxdepth 1 -mindepth 1 -type d -exec tar -czvf {}.tar.gz {} \;
	mv www/lib/*.tar.gz www/dist/

.PHONY : html-all
html-all :
	find www/lib/* -maxdepth 0 -type d -exec sh -c 'make html-single name=`basename {}`' \;

.PHONY : html-single
html-single :
	title=`head -n 1 www/lib/$$name/README.md | cut -c 3-`; \
	pandoc -s -f markdown-auto_identifiers -t html5 \
	--template=templates/model.html5 \
	--lua-filter=templates/filter.lua \
	--metadata=baseurl:$(baseurl) \
	--metadata=title:"$$title" \
	--metadata=slug:"$$name" \
	-o www/html/$$name.html www/lib/$$name/README.md
	cp www/lib/$$name/pic/* www/html/pic/

.PHONY : html-index
html-index :
	echo "" | pandoc -s -f markdown-auto_identifiers -t html5 \
	--template=templates/categories.html5 \
	--metadata-file=templates/categories.yaml \
	--metadata=baseurl:$(baseurl) \
	-o www/html/index.html

.PHONY : categories
categories :
	echo "IfcClasses:\n" > templates/categories.yaml
	find www/lib/* -maxdepth 0 -type d -exec sh -c 'make category name=`basename {}`' \;
	sed -i "s/^  Ifc.*/  -/" templates/categories.yaml

.PHONY : category
category :
	title=`head -n 1 www/lib/$$name/README.md | cut -c 3-`; \
	ifcClass=`yq r www/lib/$$name/metadata.yaml ifcClass`; \
	description=`yq r www/lib/$$name/metadata.yaml description`; \
	index=`yq r templates/categories.yaml IfcClasses.$$ifcClass | grep "title" | wc -l` ;\
	yq w -i templates/categories.yaml IfcClasses.$$ifcClass.name "$$ifcClass"; \
	yq w -i templates/categories.yaml IfcClasses.$$ifcClass.models[$$index].title "$$title"; \
	yq w -i templates/categories.yaml IfcClasses.$$ifcClass.models[$$index].slug "$$name"; \
	yq w -i templates/categories.yaml IfcClasses.$$ifcClass.models[$$index].description "$$description"
